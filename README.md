## Surrounded @ Home

The "Surrounded @ Home" describes a simple speaker surround system based on very cheap components, but at the same time tries to extract the best quality sound reproduction those components can provide through the use of software plugins. Other than the commercial hardware you need, all other software and 3d models is GPL or Creative Commons licensed.

This system was originally designed for the 2021 edition of the "Sound in Space" course at CCRMA, Stanford University.

### Hardware

The current hardware uses the following components:

1. Speakers: Logitec Z606 5.1 system (use two for creating a 7.1 array)

1. Audio Interface: Vantec (NBA-200U) or Startech (ICUSBAUDIO7D) USB 7.1 audio interface

These are very cheap options with only 16 bi D/A converters but usable without problems on any platform (Linux, Mac or Windows).

1. Extra cables: you will need to find cheap 1/8" to RCA stereo cables (the speaker system only comes with one cable, and you will need three total for the 5.1 system or four for the 7.1 system)

1. Speaker Stands: there are many options, I finally selected one of a number of very cheap light stand tripods (for example the Neewer 6ft/75 inch/190cm Photography Tripod Light Stand - this can be found in packs of three for very reasonable prices). The selection of available stands keeps changing so it takes quite a bit of time and luck to find the cheapest version. While using small light stands is the cheapest option I found there might be other hardware options that might be cheaper (I was originally going to make the stands out of PVC pipe but it turns out that was more expensive...).

The project includes a 3d printed adapter that can be either screwed on a 1/4" screw or clipped in the spigot of the light stand. The small satellite speakers hang from this adapter.

### Software

