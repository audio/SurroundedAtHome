## Surrounded @ Home

### Initial installation.

1. unpack the Z606 speakers, the Vantec or Startech audio interface, the additional cables and other hardware (light stands, etc)

1. screw on the 3d printed speaker adapters to the top of the ligth stands (depending on the light stand model you may have a cap that protects the 1/4" screw, you need to unscrew that).

1. hang the small satellite speakers from the speaker adapters and adjust the height of the stands so that the speakers are at ear level when you are sitting in the center of the space.

1. arrange the five satellite speakers on stands around the center of the listening area in a regular pentagon. Please note we are arranging the speakers in a regular array (equal azimuth angles between speakers), not in the irregular "home theater" 5.1 configuration. Use the "speaker_directions.pdf" guide (print a copy and tape it to the center of the space) to orient the speakers. The two speakers with red cables are left and right, the one with the blue (shorter) cable is the center speaker (directly in front of you) and the ones with grey (much longer) cables are the back speakers. With the cables provided you should be able to set up the speakers between 5 to 7 feet away from the center of the array (if possible try to have the same distance to all speakers from the center of the array). Connect the cables to the corresponding speaker terminals in the subwoofer unit. Pay attention to the polarity of the connection! (black wire always connected to the black terminal, the other wire connected to the red, blue or grey terminals). The subwoofer should be located in between the left and center speakers (on the floor), the length of the cables will dictate the final position.

1. connect the outputs of the Vantec or Startech audio interface to the subwoofer unit. The first 1/8" output of the audio interface labeled "front" is connected to the "DVD (5.1 CH)" "F.L" (white RCA plug) and "F.R" (red RCA plug) inputs of the subwoofer unit - NOT to the "L" and "R" inputs which are only stereo. The second 1/8" output labeled "surround" connects to "R.L" (white RCA plug) and "R.R" (red RCA plug) inputs. Finally the third 1/8" output labeled "CENTER/BASS" connects to the "CEN" (white RCA plug) and "SUB" (red RCA plug) inputs.

1. connect the USB cable to the audio interface and a USB port in your computer.

1. turn on the subwoofer unit and press the front panel input button until the display shows "dUd" (which stands for "DVD Input"). 

1. open the MAC "Audio MIDI Setup" window. You should see two audio USB entries, select the one that has outputs and change the "Format" to be "6 ch 16-bit Integer 48KHz" (otherwise you will only see two channels). Create an agregate device that includes both (output and input) USB interface entries. Select the newly created agregate device and click on "Configure Speakers" and select the "5.1 Surround" configuration from the pull down menu at the top. _WARNING:_ make sure the level in the subwoofer unit is set to 5 or less! Otherwise the test signal that the control panel generates will be very loud! After adjusting the level, click on the speaker icons in the configuration dialog, you should hear sound coming from the corresponding speaker. The left and right speakers are going to be 6dB louder than all other satellite speakers (this is going to be fixed later on). You should now be able to choose the agregate device in Ardour or Reaper (other any other software you use).

1. in the software of your choice add a pink noise source and make sure that the first 6 output channels are sent to the left, right, center, subwoofer, surround left and surround right speakers in that order (without correction left and right are going to be 6dB louder than the rest). The rest of the setup and software assumes you have connected the speakers in the right order.
