## Surrounded @ Home

## Configure Audio Interfaces

Depending on your speaker configuration you will need to set up your audio interface or interfaces so they can be used by Reaper (or other softwareg). 5.1 and 7.1 speaker arrays will use one USB audio interface (either StarTech, Vantec or similar hardware), a "with-height" 5.4.1 array will need two. In both cases we will set up the built-in audio output in the computer as an extra stereo output in an aggregate device. This will allow you to monitor your session with either speakers or binaural headphones.

### Device Configuration

First open the "Audio MIDI Setup" control panel. If this is the first time you do this you will need to switch the USB audio interface from the default stereo configuration (there will be two USB devices listed, select the one that has outputs):

<p align="center"><img src="img/audio_interface_stereo.png" width="500"></p>

Click on the "Format" popup menu:

<p align="center"><img src="img/audio_interface_select.png" width="500"></p>

Select either "6 ch" or "8ch" depending on the size of your array (with either 44.1 or 48KHz sampling rate).

<p align="center"><img src="img/audio_interface_6chan.png" width="500"></p>

<p align="center"><img src="img/audio_interface_8chan.png" width="500"></p>

If you have a 5_4_1 array do this again for the second USB audio interface (select both to have 8 output channels).

For now also make sure that the USB audio devices are not the default devices, so right click on the "Built-in Microphone" and select "Use this device for sound input", select the "Built-in Output" and select "Use This Device for Sound Output" (from this point on all alerts and system sounds will go to your laptop speakers).

Now create an aggregate device:

<p align="center"><img src="img/create_aggregate_device.png" width="500"></p>

Which is by default empty (no devices selected):

<p align="center"><img src="img/empty_aggregate_device.png" width="500"></p>

Change its name so that we can easily find it later (for 5.1 arrays I would suggest "USB_6_stereo", for 7.1 "USB_8_stereo" and for 5.4.1 "USB_10_stereo". Add one (or the only) 6 or 8 channel USB audio interface, add another one for 5.4.1 arrays and finally select the stereo "Built-In Output" device. This is how it looks for a USB_10_stereo aggregate device (two USB audio interfaces and the built-in output for a total of three devices):

<p align="center"><img src="img/aggregate_device_usb_10_stereo.png" width="500"></p>

## Install VST Plugins

You will need a few VST plugins that are currently in the "Surrounded @ Home" git repository here:

https://cm-gitlab.stanford.edu/audio/SurroundedAtHome

The best way to do this is to clone the repository to a folder in your computer. It will be easy after that to update it to the latest release as changes are made. In a "Terminal" type:

git clone https://cm-gitlab.stanford.edu/audio/SurroundedAtHome

This will clone the current repository to your computer. Change to the repository directory:

cd SurroundedAtHome

At any time you can download the latest version by typing:

git pull

### Install the Speaker Processor

This plugin will equalize the speakers and subwoofer with parametric sections and create a proper crossover for the subwoofer. In the Finder go to the "SurroundedAtHome/plugins/speaker_processor/" folder and drag the VST plugin that corresponds to your array to the "~/Library/Audio/Plug-Ins/VST/" folder. For a 5.1 array this will be "SAH_speaker_processor_5_1.vst".

### Install the Ambisonics Decoders

An Ambisonics decoder will decode an Ambisonics signal to the array of speakers. Install the ones that match your speaker configuration from the "SurroundedAtHome/plugins/ambisonics_decoder/decoders/" folder. For 5.1 arrays the file names will start with "SAH_ambdec5", for 7.1 arrays with "SAH_ambdec7" and for 5.4.1 arrays with "SAH_ambdec9". Copy all the ".vst" items that match your array size.

### Install the Ambisonics Encoders (optional)

The Ambisonics encoders will pan a mono source for the selected Ambisonics order in the full soundfield. There are other options for panning, such as the ones included in the AmbiX plugins. These include an extra Ambisonics output that is meant to be routed to an Ambisonics reverberator, and can also create the Doppler effect for moving sources with radial velocity components. You can find them in the "SurroundedAtHome/ambisonics_encoder/encoders/" folder.

### Install the Ambix set of plugins

[NEED INSTRUCTIONS HERE]

## Example Reaper Session

We will create a new example session configured for a 5.1 speaker array and ready to decode 5th order Ambisonics tracks. We could have used the MASTER track already present in the session and modify it accordingly but we are going to create a new track named "speakers" to use as a mix bus for all tracks.

Create a new project. In the Reaper "Preferences" window select the "Audio" tab and then "Device". In the "Audio Device" popup select the aggregate device you just created.

### Add Master Speaker Track

1. add a new track

1. in the new track select "Route" and change "Track Channels" to 6 (for 7.1 systems change to 8, for 5.4.1 systems change to 10 - this is the minimum number of channels, we will probably change this later)

1. click on "Master send" to turn it off (so that the output of this new track does not route to the master)

1. click on "add new hardware output" and select the first "mono" channel (1), skipping the stereo pairs at the top of the pop up list

1. in the new hardware output, click on the destination pop up (below the gain fader), select "Mono source" and select channel 1 from the pop up list

1. repeat for all 6 (or 8 or 10) hardware outputs, for each hardware output create a new one and select the right mono source. When you are done you should have 6, 8 or 10 hardware outputs, each one pointing to one mono output.

1. TESTING THE SPEAKERS: we could now easily test to see whether this is all working. First lower the level of the track a lot (we are going to add a noise generator and we don't want that to be very loud). Click on the FX button, search for "pink" and click on "Add" to add the pink noise generator to the track. If it is not already open click on the noise generator itself to bring up its GUI window. Click on the button that says "2 in 2 out" and you will have a routing window. Click on the existing connections and turn them OFF (nothing routed anywhere). Now click on the first output channel (left or right output, it does not matter). Bring up the track level slowly - you should be hearing noise coming from the left channel. Bring up the routing panel again and switch to channel 2, 3, 4 and 5 (right channel, center, surround left and finally surround right). If it all works remove the pink noise generator from the track and keep going...

1. click on the FX button on the track to add an Ambisonics decoder plugin for the desired ambisonics order (1, 2, 3 or 4). The name of the decoder for a 5.1 array will start with "SAH_ambdec5_ACN_SN3D_", just select the order you want (let's use "SAH_ambdec5_ACN_SN3D_3H0P" for a third order decoder - 3H0P means this decoder is created to match a horizontal array and decodes 3rd order signals).

1. change "Track Channels" in the track to match what the Ambisonics encoder (panner) needs, rounded up to the nearest multiple of 2. For first order you will need 4 channels, for second order 10 channels, third order 16 channels, fourth order 26 or fifth order 36.

IMPORTANT: note that I wrote "encoder", not "decoder"! We will add 5th order encoders (panners) to tracks on this session later on, so change this to 36 channels. This track has a 3rd order decoder (16 input channels) and that order is dictated by the number of speakers you have in your array. We can still encode our mix to 5th order (36 channels) for best spatial resolution, and discard the upper 20 channels when decoding for our array (but for example we can use all 36 in a binaural decoder that is described below). 

1. click on the FX button on the track and add a speaker processor plugin for the speaker configuration you have (5, 7 or 5+4 speakers). For a 5.1 array the plugin will be "SAH_speaker_processor_5_1".

1. make sure the plugins are in the right order, first the decoder and then the speaker processor.

You now have a 36 channel wide "master track" that will mix the Ambisonics outputs of all other tracks (through sends) and output the mixed and decoded result to all your speakers.

You can save time later if you save this track as a template now. To do that right click on track, select "Save tracks as track template", give it a name you can remember, for example "speakers_5_amb_o5" for a 5.1 configuration loaded with a 3rd order decoder but accepting up to 5th order input (you could, for example, change the decoder later on to a higher order if you get more speakers and nothing else needs to change in your session). This will save you tons of time later on when you create additional projects in Reaper.

## Add Master Binaural Track

We will now add a track that will decode 5th order Ambisonics to a stereo signal that you can listen to over headphones.

1. add a new track, name it "binaural_o5"

1. in the new track select "Route" and change "Track Channels" to 36 (for 5th order Ambisonics).

1. make sure the "Master send" is ON (we will use the master stereo track already present in the session to audition the output of this track)

1. add an Ambisonics binaural decoder plugin for 5th order. We will use the one included in the AmbiX plugin collection. Click on the FX button on the track and find the "ambix_binaural_o5" plugin, then click "Add" to add it to the track. In the plugin GUI click on "Open" and select a decoder preset. I normally use the "iem_cube_h2_mb_dpa" "allrad-o5" preset.

1. in the "MASTER" track (not the "binaural_o5" track you just created) click the "Route" button. By default the hardware audio output will be routed to a stereo pair in channels 1/2. You need to change that to point to the last two channels of your aggregate device. For a 5.1 array that will be channels 7/8, for 7.1 channels 9/10, etc. This has to match the aggregate device you created so that the two channels go to the Built-in Output instead of the USB audio interface.

You now have a "binaural master track" that will mix the Ambisonics outputs of all other tracks (through sends) and output the mixed and decoded result to headphones connected to your computer built-in audio output.

You can save time later if you save this track as a template now. To do that right click on track, select "Save tracks as track template", give it a name you can remember, for example "binaural_o5" for a 5th order session.

## Add Tracks for Panned Mono Content

Now we will add tracks that can be used to play back panned mono content.

1. add new track

1. in the new track select "Route" and change "Track Channels" to 36 (we will use this track to pan mono sources to 5th order Ambisonics)

1. click on "Master send" to turn it off (so that the output of this new track does not route to the master)

1. click on "add new send" and select the master track you just created (for example "speakers_5_amb_o5").

1. in the new send, click on the "Audio" pop up (below the gain fader), select "Multichannel", then "36 channels" and select "1-36".

1. click on "add new send" and select the binaural master track you just created (for example "binaural_o5").

1. in the new send, click on the "Audio" pop up (below the gain fader), select "Multichannel", then "36 channels" and select "1-36".

1. add an Ambisonics panner plugin, we will use "ambix_encoder_o5" from the AmbiX plugin collection. Click on the FX button in the track, search for the plugin and "Add" it to the track.

Like for the master track you should save the brand new track as a template so that it is easy to replicate this later on. Name the template to something that reflects the type of track you created, for example "amb_pan_o5".

Your track is now ready to pan mono content in full 3d surround to both the speakers and headphones connected directly to your computer. Drag a mono sound file into the track and try it out!

## Add Tracks for Ambisonics Content

Follow the same directions for mono content, but do not add the panner. When you are done you can drag a multichannel 5th order (36 channels) soundfile into the track and it will be mixed into the main speaker and binaural mixes.

## Testing

