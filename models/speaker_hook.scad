//
// Hook for mounting speakers on light stands
// 
// 1/4" thread version, or clip that attaches to spigot of small
// light stand
//
// Copyright 2020-2021, Fernando Lopez-Lezcano, All Rights Reserved
// nando@ccrma.stanford.edu
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// 3d Models released under the Creative Commons license as follows:
//   Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
// http://creativecommons.org/licenses/by-nc-sa/4.0/
// (C) 2020 Fernando Lopez-Lezcano
//
// Thread-drawing modules for OpenSCAD
// https://dkprojects.net/openscad-threads/
include <threads.scad>;

thin_mint = 0.001;

// diameter of hole
h_d = 3.0;
// diameter of knob
h_dh = 8.5;
// thickness of speaker wall
h_w = 2.8;

h_r = 5.0;

// body
body_w = 10.0;
body_l = 10.0;
body_t = 4.0;

// stand
// diameter of grip
s_d = 11.3;
// height of grip
//s_h = 15.0;
s_h = 8.5 + 2.0;
// wall thickness
wall = 2.0;
s_dw = s_d + 2 * wall;

// overlap for grip
over = 3.5;
// distance from grip to speaker
sep = 10.0;

// attach with clip to stand spigot
*translate([0, 0, 0]) rotate([0, -90, 0]) {
    difference() {
        union() {
            // body of grip
            hull() {
                // overhang for clipping into tube
                translate([0, 0, -over]) cube([s_h, s_dw, thin_mint]);
                cube([s_h, s_dw, thin_mint]);
                translate([0, 0, sep/3]) cube([s_h, s_dw, thin_mint]);
                translate([0, s_dw/4, s_d/2 + sep]) cube([s_h*(4/4), s_dw/2, thin_mint]);
            }
            // speaker hook
            translate([h_dh/2, s_dw/2, s_d/2 + sep]) {
                cylinder(r = h_r/2 + 1.0, h = h_d, $fn = 64);
                translate([0, 0, h_w]) cylinder(r = h_dh/2, h = h_d, $fn = 128);
            }          
        }
        // clip
        translate([-thin_mint, s_dw/2, 0]) rotate([90, 0, 90]) {
            cylinder(r = s_d/2, h = s_h + 2*thin_mint, $fn = 64);
        }
    }
}

// attach to 1/4" screw
translate([0, 0, 0]) rotate([0, -90, 0]) {
    difference() {
        union() {
            // body of grip
            hull() {
                translate([0, s_dw/4, s_d/2 + sep]) cube([s_h*(4/4), s_dw/2, thin_mint]);
                translate([0, s_dw/2, 0]) rotate([0, 90, 0]) cylinder(r = 7, h = s_h, $fn = 64);
            }
            // speaker hook (lift 1.0 from bed, very difficult to print otherwise, use support)
            translate([h_dh/2 + 1.0, s_dw/2, s_d/2 + sep]) {
                cylinder(r = h_r/2, h = h_d, $fn = 64);
                translate([0, 0, h_w]) cylinder(r = h_dh/2, h = h_d, $fn = 128);
            }          
        }
        // nut
        translate([-thin_mint, s_dw/2, 0]) rotate([90, 0, 90]) {
            english_thread (diameter=1/4, threads_per_inch=20, length=1.1/2.54 + thin_mint, internal=true);
        }
    }
}
