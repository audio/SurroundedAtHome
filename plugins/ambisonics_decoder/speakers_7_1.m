function [ val ] = speakers_7_1( )
  %% Surrounded @ Home 7.1 array
    
  val = ambi_mat2spkr_array( [

	 %% regular heptagon (LR)
         %% using the "standard" 7.1 order (minus the sub)
	 51.4	0	1.0;
	 -51.4	0	1.0;
	 0	0	1.0;
	 102.9	0	1.0;
	 -102.9	0	1.0;
	 154.3	0	1.0;
	 -154.3	0	1.0;
        ], ...
	'AER', ...
        'DDM', ...
	'amb', ...
	{ 
	  'L'; 'R'; 'C'; 'SL'; 'SR'; 'BL'; 'BR7';
	});
