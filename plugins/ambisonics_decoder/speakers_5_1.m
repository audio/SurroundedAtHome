function [ val ] = speakers_5_1( )
  %% Surrounded @ Home 5.1 array
    
  val = ambi_mat2spkr_array( [

	 %% regular pentagon (LR)
         %% using the "standard" 5.1 order (minus the sub)
	 72	0	1.0;
	 -72	0	1.0;
	 0	0	1.0;
	 144	0	1.0;
	 -144	0	1.0;
        ], ...
	'AER', ...
        'DDM', ...
	'amb', ...
	{ 
	  'L'; 'R'; 'C'; 'SL'; 'SR';
	});
