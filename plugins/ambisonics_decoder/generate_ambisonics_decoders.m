%%%
%%% Surrounded @ Home 2020
%%%
%%% create family of Ambisonics decoders for both
%%% 5.1 and 7.1 regular arrays

more off;

%%% install adt one directory up
%%% https://bitbucket.org/ambidecodertoolbox/adt.git
%%%
addpath('../adt/matlab');
addpath('../adt');

%% make a family of decoders for orders from 1 to 4 (3D)
for order = [1 2 3 4]
  fprintf('\n=== Writing ACN/SN3D PINV horizontal decoder, order %d:%d\n', order);
  C = ambi_channel_definitions(order, 0, 'HP', 'ACN', 'SN3D');
  ambi_run_pinv(...
		speakers_5_1(), ...  
		C, ...
		[0 0 -1/2], ...  % imaginary speaker at bottom of dome
		[sprintf('decoders/SAH_ambdec5_ACN_SN3D_%dH%dP', order, 0)], ...
		false, ...        % graphics
		'HP');
  fprintf('\n=== Done writing 5.1 decoder, order %d:%d\n', order);
  ambi_run_pinv(...
		speakers_7_1(), ...  
		C, ...
		[0 0 -1/2], ...  % imaginary speaker at bottom of dome
		[sprintf('decoders/SAH_ambdec7_ACN_SN3D_%dH%dP', order, 0)], ...
		false, ...        % graphics
		'HP');
  fprintf('\n=== Done writing 7.1 decoder, order %d:%d\n', order);
  %% now add SN2D versions
  C = ambi_channel_definitions(order, 0, 'HP', 'ACN', 'SN2D');
  ambi_run_pinv(...
		speakers_5_1(), ...  
		C, ...
		[0 0 -1/2], ...  % imaginary speaker at bottom of dome
		[sprintf('decoders/SAH_ambdec5_ACN_SN2D_%dH%dP', order, 0)], ...
		false, ...        % graphics
		'HP');
  fprintf('\n=== Done writing 5.1 SN2D decoder, order %d:%d\n', order);
  ambi_run_pinv(...
		speakers_7_1(), ...  
		C, ...
		[0 0 -1/2], ...  % imaginary speaker at bottom of dome
		[sprintf('decoders/SAH_ambdec7_ACN_SN2D_%dH%dP', order, 0)], ...
		false, ...        % graphics
		'HP');
  fprintf('\n=== Done writing 7.1 SN2D decoder, order %d:%d\n', order);
end
fprintf('\n');
