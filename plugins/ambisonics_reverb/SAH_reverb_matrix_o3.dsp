declare name		"SAH_reverb_matrix_o3";
declare version 	"1.0";
declare author 		"SurroundedAtHome";
declare license 	"GPL";
declare copyright	"(c) 2021 Fernando Lopez-Lezcano";

// Diffusion matrix to be inserted after the Ambisonics decoder in
// the reverberation signal chain, it allows to define the level of
// global reverberation as opposed to local

import("stdfaust.lib");

// matrix multiply:
process = par(i, spkrs, _) <: par(j, spkrs, gain(spkr_vec(j)) :> _);

spkrs = 15;

spkr_vec(0) = (local, par(j, spkrs - 1, global));
// this has to be manually configured to be one less that spkrs
spkr_vec(14) = (par(j, spkrs - 1, global), local);
spkr_vec(i) = (par(j, i, global), local, par(k, spkrs - i - 1, global));

local = hslider("local reverb [unit:dB]", 0, -70, +3, 0.1): ba.db2linear : dezipper;
global = hslider("global reverb [unit:dB]", -12, -70, +3, 0.1): ba.db2linear : dezipper;

// UI "dezipper"
smooth(c) = *(1-c) : +~*(c);
dezipper = smooth(0.999);

// bus with gains
gain(c) = R(c) with {
  R((c,cl)) = R(c),R(cl);
  R(1)      = _;
  R(0)      = !:0;
  R(float(0)) = R(0);
  R(float(1)) = R(1);
  R(c)      = *(c);
};

