%%%
%%% Surrounded @ Home
%%%
%%% Ambisonics 3rd order reverb plugin: Ambisonics decoder
%%%
%%% Copyright 2021 Fernando Lopez-Lezcano
%%%
%%%  This program is free software: you can redistribute it and/or modify
%%%  it under the terms of the GNU General Public License as published by
%%%  the Free Software Foundation, either version 3 of the License, or
%%%  (at your option) any later version.
%%%
%%%  This program is distributed in the hope that it will be useful,
%%%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%%%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%%  GNU General Public License for more details.
%%%
%%%  You should have received a copy of the GNU General Public License
%%%  along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%
%%% Create a third order Ambisonics decoder for an 8+6+1 array that mimics
%%% the upper speaker dome in the Listening Room. This is used to create a
%%% virtual speaker array for Ambisonics 3rd order reverberation. The decoded
%%% outputs are sent to a reverberation unit (impulse responses or algorithmic
%%% reverb) through a diffusion matrix and the encoded back into Ambisonics.
%%%
%%% After creating the decoder disable delay_comp, level_comp, nfc_output,
%%% nfs_input and output_gain_muting in the generated dsp file

addpath('../adt');
addpath('../adt/matlab');

function generate_decoder_o3()
  order = 3;
  
  fprintf('\n=== Writing ACN/SN3D ALLRAD dome decoder, order %d:%d\n', order, order);
  C = ambi_channel_definitions(order, order, 'HP', 'ACN', 'SN3D');
  ambi_run_allrad(...
		 dome_speakers(), ...  
		 C, ...
		 [0 0 -1/2], ...  % imaginary speaker at bottom of dome
		 [sprintf('SAH_reverb_decoder_o3')], ...
		 false, ...        % graphics
		 'HP');
  fprintf('\n=== Done writing LD dome decoder, order %d:%d\n', order);
end

function [ val ] = dome_speakers()
  %% simulated Listening Room upper dome speaker positions
  %% arranged as stereo pairs (matches SAH_encoder_o3)
  %%
  %% constant distance as we are not going to use NFC filters
  %%    
  %% azimuth (deg), elevation (deg), distance (meters)
  val.name = 'CCRMA_Listening_Room_Dome';
  S = [
       22.5   0    3
       -22.5  0    3
       67.5   0    3
       -67.5  0    3
       112.5  0    3
       -112.5 0    3
       157.5  0    3
       -157.5 0    3
       30.0   40   3
       -30.0  40   3
       90.0   40   3
       -90.0  40   3
       150.0  40   3
       -150.0 40   3
       0      90   3
  ];
  val.id = {'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'H7', 'H8', ...
            'T1', 'T2', 'T3', 'T4', 'T5', 'T6', ...
            'TT'};
  val.az = S(:,1)*pi/180;
  val.el = S(:,2)*pi/180;
  val.r = S(:,3);

  %% direction cosines, unit vector
  [val.x, val.y, val.z] = sph2cart(val.az, val.el, 1);
end
