//
// Surrounded @ Home
//
// A very low cost surround system for teaching sound spatialization topics
//
// Faust Speaker Processor for 7.1 array
//
// This plugin performs the processing necessary to use cheap multi-speaker
// surround systems directly from an audio interface, including multiple band
// parametric equalization, crossovers and speaker delays
//
// Copyright 2020-2021, Fernando Lopez-Lezcano, All Rights Reserved
// nando@ccrma.stanford.edu
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// 3d Models released under the Creative Commons license as follows:
//   Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
// http://creativecommons.org/licenses/by-nc-sa/4.0/

// derived in part from template created by Champ Darabundit 10/09/2020

import("stdfaust.lib");

process = _*L, _*R, _*C, _*SL, _*SR, _*BL, _*BR :
    (_<:_,_), (_<:_,_), (_<:_,_), (_<:_,_), (_<:_,_), (_<:_,_), (_<:_,_) :
    ro.interleave(2, 7) :
    (par(i, 7, main)) , sum(i, 7, _) :
    _, _, _, _, _, _, _, sub * SUB :
    route(8, 8, (1, 1), (2, 2), (3, 3), (8, 4), (4, 5), (5, 6), (6, 7), (7, 8))
with {
  // gain for individual channels (L/R are +6dB relative to C and SL/SR-BL/BR)
  L = 1.0 * ba.db2linear(-6.0);
  R = 1.0 * ba.db2linear(-6.0);
  C = 1.0;
  SL = 1.0;
  SR = 1.0;
  BL = 1.0;
  BR = 1.0;
  SUB = 1.0 * ba.db2linear(sub_trim);
  // crossover parameters (butterworth filters)
  xover_order = 2;
  xover_freq = 170;
  // main speaker processing
  //   delay -> highpass crossover filter -> equalizer -> tilt
  main = de.sdelay(0.2/ma.SR, 1024, (MAIN_DELAY/1000)/ma.SR) :
       ba.bypass1(EQ, main_chan) :
       ba.bypass1(XOM, fi.highpass(xover_order, xover_freq)) :
       fi.spectral_tilt(3, 8000, 12000, TILT);
  // subwoofer processing
  //   delay -> lowpass crossover filter -> equalizer
  sub = de.sdelay(0.2/ma.SR, 1024, (SUB_DELAY/1000)/ma.SR) :
      ba.bypass1(EQ, sub_chan) : ba.bypass1(XOS, fi.lowpass(xover_order, xover_freq));
  // enable main speaker crossover filter
  XOM = checkbox("v:common/[10]MAIN XOVER disable");
  // enable subwoofer crossover filter
  XOS = checkbox("v:common/[10]SUB XOVER disable");
  // enable parametric equalizer
  EQ = checkbox("v:common/[11]EQ disable");
  // delay of main speaker signals
  MAIN_DELAY = hslider("v:common/[12]MAIN DELAY", 0, 0, 200, 0.1);
  // delay of subwoofer signal
  SUB_DELAY = hslider("v:common/[12]SUB DELAY", 0, 0, 200, 0.1);
  // high frequency spectral tilt
  TILT = hslider("v:common/[13]TILT", 0, -1, 1, 0.01);
  // relative level of sub
  sub_trim = hslider("v:common/[14]SUB LEV [unit:dB]
        [tooltip: Boost or cut in dB]",
        0,-40,10,0.01);
  // bank of parametric equalizers for main speakers
  main_chan = hgroup("MAIN",
    fi.peak_eq_cq(-6.50, 204.0, 2.598)
    :fi.peak_eq_cq(1.00, 680.0, 7.261)
    :fi.peak_eq_cq(-2.90, 1448, 1.943)
    :fi.peak_eq_cq(-12.20, 2726, 1.187)
    :fi.peak_eq_cq(9.00, 3301, 2.012)
    :fi.peak_eq_cq(1.60, 7163, 1.036)
    :fi.peak_eq_cq(-2.30, 10859, 2.914)
  );
  // parametric equalizer for sub
  sub_chan = hgroup("SUB",
    fi.peak_eq_cq(-10.30, 82.60, 2.095)
    :fi.peak_eq_cq(-10.20, 135.5, 2.000)
    :fi.peak_eq_cq(-10.80, 188.5, 2.000)
    :fi.peak_eq_cq(-9.20, 282.0, 2.882)
    :fi.peak_eq_cq(-10.10, 600.0, 1.000)
  );
};

