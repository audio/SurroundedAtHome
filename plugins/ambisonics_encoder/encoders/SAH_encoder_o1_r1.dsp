declare name		"SAH_encoder_o1_r1";
declare version 	"1.0";
declare author 		"AmbisonicDecoderToolkit";
declare license 	"GPL";
declare copyright	"(c) Aaron J. Heller 2013, Fernando Lopez-Lezcano 2021";

//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import("stdfaust.lib");

// ambisonics order to generate (has to match "pg" below)
N = 1;
// ambisonics order for reverberated outputs
NR = 1;

// generate a REAPER compatible plugin (number of channels in direct and reverb outs a multiple of 2)
REAPER = 1;

// number of output signals
out_chans = (N+1)^2;
rev_out_chans = (NR+1)^2;
// set to one if this is for reaper and order of direct signal is odd (2nd/4th order)
// for direct sound encoder
reaper_d = 0;
// for reverb sound encoder
// reaper_r = ba.if(ma.modulo(rev_out_chans, 2)==0, 0, 1);
reaper_r = 0;

speed_of_sound = 430.2;

process = ambix_panner(N);

ambix_panner(N) = _ : de.fdelay(max_dist, (dist/speed_of_sound)*ma.SR), test_sig : select2(test_ui) <: gain(pg) :
		par(i, out_chans, _<:_*d_scl, ambi_rev_scale(i)) :
		route(out_chans + rev_out_chans, out_chans + rev_out_chans + reaper_d + reaper_r,
		      par(i, out_chans + rev_out_chans + reaper_d + reaper_r + 1, spaguetti(ba.if(i>(rev_out_chans*2), 2, ma.modulo(i, 2)==0), i)))
with {

  // button for pink noise test signal
  test_ui = button("[99]pink noise");
  test_sig = pink(no.noise):gain(1/4);

  // azimuth and elevation angles, smooth to "dezipper" controls.
  az = (ma.PI/180.0)*hslider("[1]azi[unit:deg]", 0, -180, 180, 1) : dezipper;
  el = (ma.PI/180.0)*hslider("[2]ele[unit:deg]", 0,  -90,  90, 1) : dezipper;

  // direct sound attenuation power exponent
  direct_power = 1.5;
  // reverb sound attenuation power exponent
  reverb_power = 0.5;
  // minimum distance for object
  min_dist = 1.0;
  // limit maximum distance of slider based on max direct sound attenuation allowed
  max_dist_att = -50;
  max_dist = (1/ba.db2linear(max_dist_att))^(1/direct_power);
  // distance, smooth to dezipper
  dist = min_dist + hslider("[3]dist[unit:meter]", 0, 0, max_dist, 0.01) : dezipper;
  // direct sound and reverb scalers
  d_scl = 1/(dist^direct_power);
  r_scl = rev_amount * 1/(dist^reverb_power);
  // reverb amount, smooth to dezipper
  rev_amount = ba.db2linear(hslider("[4]reverb[unit:dB]", -20, -80, 0, 0.01) : dezipper);

  // spherical to cartesian
  r = 1;
  x = r * cos(az)*cos(el);
  y = r * sin(az)*cos(el);
  z = r * sin(el);

  // routing for outputs
  // first direct outputs
  spaguetti(0, i) = (i, i/2 + 1);
  // reverb outputs (shifted one if direct is 2nd or 4th order)
  spaguetti(1, i) = (i, (i/2) + out_chans + reaper_d);
  // the rest of the direct outputs
  spaguetti(2, i) = (i, i - rev_out_chans);

  ambi_rev_scale(i) = gate(i<((NR+1)^2))
  with {
	gate(1) = _*r_scl;
	gate(0) = !;
	};

  ambi_order_gates(N) = par(i, math.count(pg), gate(i<N))
  with{
	gate(1) = _;
	gate(0) = !;
	};

  // panner gains ACN order, SN3D normalization
  pg = (
        1,
        y,
        z,
        x
       );
};

// gain bus
gain(c) = R(c) with {
  R((c,cl)) = R(c),R(cl);
  R(1)      = _;
  R(0)      = !:0; // need to preserve number of outputs, faust will optimize away
  R(float(0)) = R(0);
  R(float(1)) = R(1);
  R(c)      = *(c);
};

pink = f : (+ ~ g) with {
  f(x) = 0.04957526213389*x - 0.06305581334498*x' + 0.01483220320740*x'';
  g(x) = 1.80116083982126*x - 0.80257737639225*x';
};

// UI "dezipper"
smooth(c) = *(1-c) : +~*(c);
dezipper = smooth(0.999);

